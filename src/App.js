import './App.css';
import RGBForm from "./Components/RGBForm"
import React, { useState } from 'react';
import DesignGrid from './Components/DesignGrid';


function App() {

  const [rgb_r, setRed] = useState(0);
  const [rgb_g, setGreen] = useState(0);
  const [rgb_b, setBlue] = useState(0);
  const [tolerance, setTolerance] = useState(0);

  function renderGrid(r, g, b, tolerance){
    
    setRed(r)
    setGreen(g)
    setBlue(b)
    setTolerance(tolerance)
  }

  return(
    <>
        <RGBForm onSubmit={renderGrid}/>
      <div>
        <DesignGrid rgb_r={rgb_r} rgb_g={rgb_g} rgb_b={rgb_b} tolerance={tolerance}/>
      </div>
    </>
  );
}

export default App;