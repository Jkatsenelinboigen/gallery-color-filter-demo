import ImageListItem from '@mui/material/ImageListItem';
import React from 'react';
import {Container} from '@mui/material';
import { Grid } from '@material-ui/core';


function DesignSquare({tile}){
   
    return(
      <Grid item xs={3} sx={{maxHeight:"100vh", minHeight:"40vh", borderBottom:1}}>
        
        {/* main variation image */}
        <ImageListItem key={tile.variations[0].img}  sx={{maxWidth:'60%', minHeight:'45vh'}}>
              <img src={tile.variations[0].img} alt={tile.uniqueEntityId}/>
        </ImageListItem>
        
        {/* variation swatch */}
        <Container sx={{borderTop:2, maxHeight:'10vh', overflowX:"auto" }}>
          {tile.variations.map( variation => (
            <ImageListItem key={variation.img}  sx={{width:'25%', paddingRight:'5%'}} >
              <img src={variation.img} alt={variation.token} title={"Distance to target: " + variation.distanceToTarget}/>
            </ImageListItem>
          ))}
        </Container>
      </Grid>
    )
  }

  export default DesignSquare;