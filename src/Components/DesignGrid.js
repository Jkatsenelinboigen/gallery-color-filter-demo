import ColorFilterQuery from './../ColorFilterQuery';
import DesignSquare from './DesignSquare';
import React, { useState, useEffect } from 'react';
import { Grid, Paper } from '@material-ui/core';

function DesignGrid({rgb_r, rgb_g, rgb_b, tolerance}){ 

    const [tileData, setTileData] = useState([]);
    const [queryTimes, setQueryTimes] = useState([]);

    useEffect(() => {
      const fetchTiles = async () =>{
        var ColorFilterResults = new ColorFilterQuery(rgb_r, rgb_g, rgb_b, tolerance)
        
        const rawTileData = (await ColorFilterResults.getJSONResultSet());

          console.log(rawTileData)
          
          var queryTimes = rawTileData.data.queryTimesMs;
          setQueryTimes([queryTimes.solrQueryTimeMs, queryTimes.sortResultsTimeMs, queryTimes.totalSearchTimeMs])
          
          var outputTiles = [];
          var cVariation = [];
          rawTileData.data.results.map(
            entity =>(
              // console.log(entity),
              console.log("Populating tile data..."),

              entity.variations.map(variation =>
                cVariation.push({
                  distanceToTarget: variation.distanceToTarget,
                  token: variation.templateToken,
                  colorSchema: variation.colorConversionSchema,
                  img: variation.previewURL
                })),
                
                outputTiles.push(
                  {
                    uniqueEntityId: entity.uniqueEntityId,
                    numVariations: entity.numVariations,
                    variations: cVariation
                  }
                ),
                cVariation = []
            )
          )

          setTileData(outputTiles);
      }
      fetchTiles();
    }, [rgb_r, rgb_g, rgb_b, tolerance] );


    if(tileData.length === 0){
      return (<>
          <Paper><b>Solr Query time: </b>{queryTimes[0]}ms | <b>Result manipulation time:</b> {queryTimes[1]}ms |<b> Total time: </b>{queryTimes[2]}ms</Paper>
          <div>No Designs Found...</div>
        </>
      )
    }

    return(
      <>
        <Paper><b>Solr Query time: </b>{queryTimes[0]}ms | <b>Result manipulation time:</b> {queryTimes[1]}ms |<b> Total time: </b>{queryTimes[2]}ms</Paper>
        <div>Found {tileData.length} Designs</div>
        
        <div>
          <Grid container spacing={1}  sx={{paddingTop:8, height: '80vh', overflowX:"auto"}}>
            {tileData.map(tile => (
              <DesignSquare key={tile.token} tile={tile}/>
            ))}
          </Grid>
        </div>
      </>
    )

    
}


export default DesignGrid;