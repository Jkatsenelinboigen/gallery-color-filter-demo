import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import ColorPicker from 'material-ui-color-picker'
import Card from '@mui/material/Card';

function RGBForm({onSubmit}){
    const [rgb_r, setRed] = useState(0);
    const [rgb_g, setGreen] = useState(0);
    const [rgb_b, setBlue] = useState(0);
    const [tolerance, setTolerance] = useState(0);
    const [colorSpace, setColorSpace] = useState("LAB");

    const handleSubmit = (event) => {
      event.preventDefault();
      onSubmit(rgb_r, rgb_g, rgb_b, tolerance, colorSpace)
    }
    return (
      <form onSubmit={handleSubmit} sx={{height:'25vh'}}>
        <ColorPicker
          name='color'
          defaultValue='Choose a Color'
          sx={{width:150}}
          onChange={color => setRgbStateFromHexCode(color)}
        />
        
        <div/>
        <label>Red:
          <TextField sx={{width:120}} value={rgb_r} onChange={(e) => setRed(e.target.value)} />
        </label>
        <label>Green:
          <TextField sx={{width:120}} value={rgb_g} onChange={(e) => setGreen(e.target.value)} />
        </label>
        <label>Blue:
          <TextField sx={{width:120}} value={rgb_b} onChange={(e) => setBlue(e.target.value)} />
        </label>
        <label>Tolerance (0 is exact match):
          <TextField sx={{width:120}} value={tolerance}  onChange={(e) => setTolerance(e.target.value)} />
        </label>
        <input type="submit" />
        <Card sx={{background:`rgb(${rgb_r}, ${rgb_g}, ${rgb_b})`}}>Selected Color</Card>
      </form>  
    );

    function setRgbStateFromHexCode(hex) {

      //converts hex code to RGB  
      var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      
      var r = parseInt(result[1], 16);
      var g = parseInt(result[2], 16);
      var b = parseInt(result[3], 16);
      setRed(r);
      setGreen(g);
      setBlue(b);
    } 
  }



  export default RGBForm;