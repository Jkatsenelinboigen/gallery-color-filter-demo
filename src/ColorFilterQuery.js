
class ColorFilterQuery{

    constructor(R, G, B, tolerance) {
        this.resultsURL = `http://localhost:5004/fetch?r=${R}&g=${G}&b=${B}&tolerance=${tolerance}`;
    }

    async getJSONResultSet(){
        const axios = require('axios');
        const response =  await axios.get(this.resultsURL);
        console.log(response)
        return  response;
    }
 
}

export default ColorFilterQuery;